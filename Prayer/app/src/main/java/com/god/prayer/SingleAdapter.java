package com.god.prayer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Harsh on 22-06-2017.
 */

public class SingleAdapter extends RecyclerView.Adapter<SingleAdapter.SingleViewHolder> {

    GetItemPostionOnClick getItemPostionOnClick;

    ArrayList<String> data;

    public SingleAdapter(ArrayList<String> data){
        this.data = data;
    }


    @Override
    public SingleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_view, parent, false);

        SingleViewHolder myViewHolder = new SingleViewHolder(itemView);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(SingleViewHolder holder, final int position) {
        holder.text.setText(data.get(position));
        holder.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int postion) {
                getItemPostionOnClick.getItemPostionOnClick(view,position,0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setGetItemPostionOnClick(GetItemPostionOnClick getItemPostionOnClick) {
        this.getItemPostionOnClick = getItemPostionOnClick;
    }

    public class SingleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        OnItemClickListener onItemClickListener;

        TextView text;

        public SingleViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.single_text);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(onItemClickListener!=null){
                onItemClickListener.onItemClick(view,getAdapterPosition());
            }
        }

        public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }
    }
}
