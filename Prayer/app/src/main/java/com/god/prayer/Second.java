package com.god.prayer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by Harsh on 21-06-2017.
 */

public class Second extends Fragment{

    RecyclerView prayersrv;

    ArrayList<Prayer> prayers ;
    PrayerAdapter prayerAdapter;

    datahandler datahandler;

    public Second(){

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_second, container, false);

        datahandler = new datahandler(getActivity());

        prayers = new ArrayList<>();

        prayersrv = (RecyclerView) rootView.findViewById(R.id.prayersrv);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        prayerAdapter = new PrayerAdapter(prayers);
        prayersrv.setLayoutManager(layoutManager);
        prayersrv.setItemAnimator(new DefaultItemAnimator());
        prayersrv.setAdapter(prayerAdapter);

        prayerAdapter.setGetItemPostionOnClick(new GetItemPostionOnClick() {
            @Override
            public void getItemPostionOnClick(View view, int position, int other) {
                Prayer prayer = prayers.get(position);
                if(other==0) {
                    if (prayer.isFav()) {
                        datahandler.UpdateQuery("false", prayer.getId());
                        setdata();
                    } else {
                        datahandler.UpdateQuery("true", prayer.getId());
                        setdata();
                    }
                }
                else if(other ==1){
                    int id = prayer.getId();
                    Bundle data = new Bundle();
                    data.putInt("id",id);
                    Intent prayerintent = new Intent(getActivity(),PrayerActivity.class);
                    prayerintent.putExtras(data);
                    startActivity(prayerintent);
                }
            }
        });

        setdata();

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        setdata();
    }

    public void setdata(){
        prayers.clear();
        prayers.addAll(datahandler.getFav());
        prayerAdapter.notifyDataSetChanged();
    }
}
