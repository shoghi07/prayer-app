package com.god.prayer;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

/**
 * Created by Harsh on 03-07-2017.
 */

public class datahandler extends SQLiteAssetHelper {

    static final String DATABASENAME = "prayers.sqlite";
    static final int VERSION = 1;
    public static final String ID = "id";
    public static final String PRAYER = "prayer";
    public static final String CATEGORY = "catgory";
    public static final String FAV = "fav";

    public datahandler(Context context) {
        super(context, DATABASENAME, null, VERSION);
    }
    public ArrayList<Prayer> getPrayers(){
        SQLiteDatabase db = getWritableDatabase();
        Cursor res =  db.rawQuery("SELECT * FROM prayers", null);
        res.moveToFirst();

        ArrayList<Prayer> prayers=new ArrayList<>();

        while(res.isAfterLast() == false){
            Prayer prayer=new Prayer(res.getInt(res.getColumnIndex(ID)),
                    res.getString(res.getColumnIndex(PRAYER)),
                    res.getString(res.getColumnIndex(CATEGORY)),
                    res.getString(res.getColumnIndex(FAV)).equals("true"));

            prayers.add(prayer);
            res.moveToNext();
        }
        return prayers;
    }
    public ArrayList<Prayer> getPrayersByCat(String cat){
        SQLiteDatabase db = getWritableDatabase();
        Cursor res =  db.rawQuery("SELECT * FROM prayers WHERE catgory ='"+cat+"'", null);
        res.moveToFirst();

        ArrayList<Prayer> prayers=new ArrayList<>();

        while(res.isAfterLast() == false){
            Prayer prayer=new Prayer(res.getInt(res.getColumnIndex(ID)),
                    res.getString(res.getColumnIndex(PRAYER)),
                    res.getString(res.getColumnIndex(CATEGORY)),
                    res.getString(res.getColumnIndex(FAV)).equals("true"));

            prayers.add(prayer);
            res.moveToNext();
        }
        return prayers;
    }
    public ArrayList<String> getCategory(){
        SQLiteDatabase db  =  getWritableDatabase();
        Cursor res =  db.rawQuery("SELECT DISTINCT catgory FROM prayers", null);

        res.moveToFirst();

        ArrayList<String> categories = new ArrayList<>();

        while(res.isAfterLast() == false){
            categories.add(res.getString(res.getColumnIndex(CATEGORY)));
            res.moveToNext();
        }
        return categories;
    }
    public void UpdateQuery(String value,int id){
        SQLiteDatabase db  =  getWritableDatabase();
        db.execSQL("UPDATE prayers SET fav = '"+value+"' WHERE id ="+id);

    }
    public ArrayList<Prayer> getFav(){
        SQLiteDatabase db = getWritableDatabase();
        Cursor res =  db.rawQuery("SELECT * FROM prayers WHERE fav ='true'", null);
        res.moveToFirst();

        ArrayList<Prayer> prayers=new ArrayList<>();

        while(res.isAfterLast() == false){
            Prayer prayer=new Prayer(res.getInt(res.getColumnIndex(ID)),
                    res.getString(res.getColumnIndex(PRAYER)),
                    res.getString(res.getColumnIndex(CATEGORY)),
                    res.getString(res.getColumnIndex(FAV)).equals("true"));

            prayers.add(prayer);
            res.moveToNext();
        }
        return prayers;
    }
    public Prayer getPrayerbyID(int id){
        SQLiteDatabase db = getWritableDatabase();
        Cursor res =  db.rawQuery("SELECT * FROM prayers WHERE id ="+id+"", null);
        res.moveToFirst();
        Prayer prayer=new Prayer(res.getInt(res.getColumnIndex(ID)),
                res.getString(res.getColumnIndex(PRAYER)),
                res.getString(res.getColumnIndex(CATEGORY)),
                res.getString(res.getColumnIndex(FAV)).equals("true"));

        res.moveToNext();
        return prayer;
    }

}
