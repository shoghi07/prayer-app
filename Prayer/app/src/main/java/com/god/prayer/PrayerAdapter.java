package com.god.prayer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import de.morrox.fontinator.FontTextView;

/**
 * Created by Harsh on 28-06-2017.
 */

public class PrayerAdapter extends RecyclerView.Adapter<PrayerAdapter.PrayerView> {

    GetItemPostionOnClick getItemPostionOnClick;

    ArrayList<Prayer> prayers;

    public PrayerAdapter(ArrayList<Prayer> prayers) {
        this.prayers = prayers;
    }

    @Override
    public PrayerView onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.prayer_item_view, parent, false);

        PrayerView myViewHolder = new PrayerView(itemView);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final PrayerView holder, final int position) {
        final Prayer prayer = prayers.get(position);
        if(prayer.getPrayer().length()>100){
            holder.prayer.setText(prayer.getPrayer().substring(0,100)+"...");
        }
        else{
            holder.prayer.setText(prayer.getPrayer());
        }
        if(prayer.isFav()){
            holder.indicator.setImageResource(R.drawable.stars);
        }
        else{
            holder.indicator.setImageResource(R.drawable.star);
        }

        holder.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int postion) {
                //update the fav variable in sql lite
                if(getItemPostionOnClick!=null){
                    if(view==holder.indicator)
                        getItemPostionOnClick.getItemPostionOnClick(view,postion,0);
                    else if(view==holder.prayer)
                        getItemPostionOnClick.getItemPostionOnClick(view,postion,1);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return prayers.size();
    }

    public void setGetItemPostionOnClick(GetItemPostionOnClick getItemPostionOnClick) {
        this.getItemPostionOnClick = getItemPostionOnClick;
    }

    public class PrayerView extends RecyclerView.ViewHolder implements View.OnClickListener{

        OnItemClickListener onItemClickListener;

        FontTextView prayer;
        ImageView indicator;

        public PrayerView(View itemView) {
            super(itemView);

            prayer = (FontTextView) itemView.findViewById(R.id.prayer_text);
            indicator = (ImageView) itemView.findViewById(R.id.favorite_indicator);
            indicator.setOnClickListener(this);
            prayer.setOnClickListener(this);
        }

        public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }

        @Override
        public void onClick(View view) {
            if(onItemClickListener!=null){
                onItemClickListener.onItemClick(view,getAdapterPosition());
            }
        }
    }
}
