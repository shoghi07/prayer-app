package com.god.prayer;

/**
 * Created by Harsh on 28-06-2017.
 */

public class Prayer {
    int id;
    String prayer;
    String category;
    boolean fav;

    public Prayer(int id, String prayer, String category, boolean fav) {
        this.id = id;
        this.prayer = prayer;
        this.category = category;
        this.fav = fav;
    }

    public int getId() {
        return id;
    }

    public String getPrayer() {
        return prayer;
    }

    public String getCategory() {
        return category;
    }

    public boolean isFav() {
        return fav;
    }
}
