package com.god.prayer;

import android.os.TestLooperManager;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class PrayerActivity extends AppCompatActivity {

    datahandler datahandler;

    TextView prayer_text;
    ImageView favorite_indicator;
    Prayer prayer;
    int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prayer);

        getSupportActionBar().setTitle("Prayers");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        datahandler = new datahandler(PrayerActivity.this);

        prayer_text = (TextView) findViewById(R.id.prayer_text);
        favorite_indicator = (ImageView) findViewById(R.id.favorite_indicator);

        Bundle data = getIntent().getExtras();
        id = data.getInt("id");

        setdata();
        favorite_indicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (prayer.isFav()) {
                    datahandler.UpdateQuery("false", prayer.getId());
                    setdata();
                } else {
                    datahandler.UpdateQuery("true", prayer.getId());
                    setdata();
                }
            }
        });
    }

    private void setdata() {

        prayer = datahandler.getPrayerbyID(id);

        prayer_text.setText(prayer.getPrayer());

        if(prayer.isFav()){
            favorite_indicator.setImageResource(R.drawable.stars);
        }
        else{
            favorite_indicator.setImageResource(R.drawable.star);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        else
            return super.onOptionsItemSelected(item);
    }

}
