package com.god.prayer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Harsh on 21-06-2017.
 */

public class First extends Fragment {

    ArrayList<String> categories;
    datahandler datahandler;

    public First(){
        categories = new ArrayList<>();

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        datahandler = new datahandler(getActivity());

        RecyclerView catgoryrv = (RecyclerView) rootView.findViewById(R.id.categoryrv);

        SingleAdapter adapter = new SingleAdapter(categories);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());


        catgoryrv.setLayoutManager(layoutManager);
        catgoryrv.setItemAnimator(new DefaultItemAnimator());
        catgoryrv.setAdapter(adapter);

        categories.clear();
        categories.addAll(datahandler.getCategory());

        adapter.notifyDataSetChanged();

        adapter.setGetItemPostionOnClick(new GetItemPostionOnClick() {
            @Override
            public void getItemPostionOnClick(View view, int position, int other) {
                Intent prayerlist = new Intent(getActivity(),PrayersActivity.class);
                Bundle data = new Bundle();
                data.putString("cat",categories.get(position));
                prayerlist.putExtras(data);
                startActivity(prayerlist);
            }
        });

        return rootView;
    }
}
