package com.god.prayer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

public class PrayersActivity extends AppCompatActivity {

    RecyclerView prayersrv;

    ArrayList<Prayer> prayers ;

    datahandler datahandler;

    String category;
    PrayerAdapter prayerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prayers);

        getSupportActionBar().setTitle("Prayers");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle data = getIntent().getExtras();

        category = data.getString("cat");

        datahandler =  new datahandler(PrayersActivity.this);

        prayers = new ArrayList<>();

        prayersrv = (RecyclerView) findViewById(R.id.prayersrv);

        LinearLayoutManager layoutManager = new LinearLayoutManager(PrayersActivity.this);

        prayerAdapter = new PrayerAdapter(prayers);
        prayersrv.setLayoutManager(layoutManager);
        prayersrv.setItemAnimator(new DefaultItemAnimator());
        prayersrv.setAdapter(prayerAdapter);

        prayerAdapter.setGetItemPostionOnClick(new GetItemPostionOnClick() {
            @Override
            public void getItemPostionOnClick(View view, int position, int other) {
                Prayer prayer = prayers.get(position);
                if(other==0) {
                    if (prayer.isFav()) {
                        datahandler.UpdateQuery("false", prayer.getId());
                        setdata();
                    } else {
                        datahandler.UpdateQuery("true", prayer.getId());
                        setdata();
                    }
                }
                else if(other ==1){
                    int id = prayer.getId();
                    Bundle data = new Bundle();
                    data.putInt("id",id);
                    Intent prayerintent = new Intent(PrayersActivity.this,PrayerActivity.class);
                    prayerintent.putExtras(data);
                    startActivity(prayerintent);
                }
            }
        });

        setdata();
    }

    public void setdata(){
        prayers.clear();
        prayers.addAll(datahandler.getPrayersByCat(category));
        prayerAdapter.notifyDataSetChanged();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        }
        else
            return super.onOptionsItemSelected(item);
    }
}
